<?php

namespace Atomic;

use DI\Bridge\Slim\App as DIBridge;
use DI\ContainerBuilder;

class App extends DIBridge
{
    protected function configureContainer(ContainerBuilder $builder)
    {
        $builder->addDefinitions([
            'settings.displayErrorDetails' => false,
        ]);
        $builder->addDefinitions(__DIR__.'/../config/container.php');
    }
}

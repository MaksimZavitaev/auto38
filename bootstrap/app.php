<?php

use Atomic\App;
use Dotenv\Dotenv;
use Illuminate\Database\Capsule\Manager as Capsule;
use Noodlehaus\Config;

define('ROOT_PATH', dirname(__DIR__));
define('APP_PATH', ROOT_PATH.'/app');
define('PUBLIC_PATH', ROOT_PATH.'/public');

session_start();

require ROOT_PATH.'/vendor/autoload.php';

$dotenv = new Dotenv(ROOT_PATH);
$dotenv->load();

$app = new App();
$container = $app->getContainer();
$config = new Config(ROOT_PATH.'/config/app.php');
$db = $config->get('db');

$capsule = new Capsule();
$capsule->addConnection($db);

$capsule->setAsGlobal();
$capsule->bootEloquent();

require APP_PATH.'/routes.php';
